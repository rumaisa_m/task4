package com.example.myapplication;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NextActivity extends AppCompatActivity {

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nextactivity);


        Button submit = (Button) findViewById(R.id.button2);
        TextView tv2 = (TextView)findViewById(R.id.textView);
        TextView tv3 = (TextView)findViewById(R.id.textView2);
        TextView tv4 = (TextView)findViewById(R.id.textView3);

        Intent intent = getIntent();

        String str = intent.getStringExtra("first");
        tv2.setText("First Name: " + str);
        String str2 = intent.getStringExtra("last");
        tv3.setText("Last Name: " + str2);
        String str3 = intent.getStringExtra("id");
        tv4.setText("Email: " + str3);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
